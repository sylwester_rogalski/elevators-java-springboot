package com.tingco.codechallenge.elevator.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.awaitility.Awaitility;
import com.tingco.codechallenge.elevator.ElevatorApplication;
import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorController;
import com.tingco.codechallenge.elevator.api.impl.ElevatorControllerImpl;
import com.tingco.codechallenge.elevator.api.impl.ElevatorImpl;
import com.tingco.codechallenge.elevator.dto.ElevatorRequestDto;
import com.tingco.codechallenge.elevator.dto.ElevatorResponseDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import javax.xml.ws.Response;

import java.util.Optional;

import static com.jayway.awaitility.Awaitility.await;
import static com.jayway.awaitility.Awaitility.fieldIn;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Boiler plate config class to get up and running with a config faster.
 *
 * @author Sven Wesley
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = ElevatorApplication.class)
@AutoConfigureMockMvc
@AutoConfigureJsonTesters
@TestPropertySource(locations = "classpath:test.properties")
public class ElevatorRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    ApplicationContext applicationContext;

    private ElevatorController elevatorController;

    private final static String PREFIX_URL_REST_API_V1 = "/rest/v1/";
    private final static String ELEVATORS_ENDPOINT = PREFIX_URL_REST_API_V1 + "elevators";
    private final static String PING_ENDPOINT = PREFIX_URL_REST_API_V1 + "ping";
    private final static String REQUEST_ELEVATOR_ENDPOINT = PREFIX_URL_REST_API_V1 + "request";

    @Before
    public void setup() {
        elevatorController = applicationContext.getBean(ElevatorControllerImpl.class);
    }

    @Test
    public void ping() throws Exception {
        mockMvc.perform(get(PING_ENDPOINT))
                .andExpect(status().isOk())
                .andExpect(content().string("pong"));
    }

    /**
     * Test if correct number elevators is created and returned via REST api
     *
     * @throws Exception
     */
    @Test
    public void checkElevatorsCreation() throws Exception {

        MvcResult result = mockMvc.perform(get(ELEVATORS_ENDPOINT))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(5)))
                .andReturn();

        System.out.println(result.getResponse().getContentAsString());
    }

    /**
     * Check if request for elevator is correctly handled
     *
     * @throws Exception
     */
    @Test
    public void checkRequestingElevator() {
        Integer addressedFloor = 9;
        ElevatorRequestDto elevatorRequestDto = new ElevatorRequestDto(addressedFloor);

        ResponseEntity<ElevatorResponseDto> responseEntity =
                testRestTemplate.postForEntity(REQUEST_ELEVATOR_ENDPOINT, elevatorRequestDto, ElevatorResponseDto.class);

        Elevator chosenElevator = elevatorController.getElevators().stream().filter(elevator -> elevator.getId() == responseEntity.getBody().getId()).findFirst().get();

        await().until(fieldIn(chosenElevator).ofType(Integer.class).andWithName("currentFloor"), equalTo(addressedFloor));

        System.out.println(responseEntity.getBody().toString());
    }


    //more tests need to be added
    //checking what happens if there are many requests at the same time
    //checking cases where all elevators need to be used
}
