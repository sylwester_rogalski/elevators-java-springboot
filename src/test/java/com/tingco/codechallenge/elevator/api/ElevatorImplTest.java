package com.tingco.codechallenge.elevator.api;

import com.tingco.codechallenge.elevator.ElevatorApplication;
import com.tingco.codechallenge.elevator.api.impl.ElevatorImpl;
import com.tingco.codechallenge.elevator.config.AppConfig;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ElevatorApplication.class)
public class ElevatorImplTest {

    @Autowired
    BeanFactory beanFactory;

    private Elevator createElevator() {
        return new ElevatorImpl(beanFactory.getBean(AppConfig.class));
    }

    @Test
    public void elevatorCreationShouldPass() {
        Elevator elevator1 = createElevator();
        Elevator elevator2 = createElevator();

        assertThat(elevator2.getId()).isEqualTo(elevator1.getId() + 1);
    }

    @Test(expected = ComparisonFailure.class)
    public void elevatorCreationShouldFail() {
        Elevator elevator1 = createElevator();

        assertThat(elevator1.getId()).isEqualTo(0);
    }

    @Test(timeout = 5000)
    public void elevatorBusinessCheck() throws InterruptedException {
        Elevator elevator1 = createElevator();

        assertThat(elevator1.isBusy()).isEqualTo(false);

        elevator1.setAddressedFloors(9);

        Executor poolExecutor = Executors.newSingleThreadExecutor();
        poolExecutor.execute(elevator1);

        Thread.sleep(1000); //should be replaced by using awaitility and checking business

        assertThat(elevator1.isBusy()).isEqualTo(true);
    }

    @Test(timeout = 5000)
    public void elevatorCheckIfGotTarget() throws InterruptedException {
        Elevator elevator1 = createElevator();

        assertThat(elevator1.isBusy()).isEqualTo(false);

        elevator1.setAddressedFloors(3);

        Executor poolExecutor = Executors.newSingleThreadExecutor();
        poolExecutor.execute(elevator1);

        Thread.sleep(4000);

        assertThat(elevator1.getCurrentFloor()).isEqualTo(3);
    }

    @Test(timeout = 5000)
    public void elevatorCheckIfMovingUpwardAndDownward() throws InterruptedException {
        Elevator elevator1 = createElevator();

        assertThat(elevator1.isBusy()).isEqualTo(false);

        elevator1.setAddressedFloors(2);

        Executor poolExecutor = Executors.newSingleThreadExecutor();
        poolExecutor.execute(elevator1);

        Thread.sleep(3000);

        elevator1.setAddressedFloors(0);

        Thread.sleep(1000);

        assertThat(elevator1.getDirection()).isEqualTo(Elevator.Direction.DOWN);
    }
}
