package com.tingco.codechallenge.elevator.controller;

import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorController;
import com.tingco.codechallenge.elevator.dto.ElevatorResponseDto;
import com.tingco.codechallenge.elevator.dto.ElevatorRequestDto;
import com.tingco.codechallenge.elevator.dto.SystemSnapshotDto;
import com.tingco.codechallenge.elevator.services.MapValidationErrorService;
import com.tingco.codechallenge.elevator.validators.ElevatorRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Rest Resource.
 *
 * @author Sven Wesley
 *
 */
@RestController
@RequestMapping("/rest/v1")
public final class ElevatorRestController {

    @Autowired
    ElevatorController elevatorController;

    @Autowired
    ElevatorRequestValidator elevatorRequestValidator;

    @Autowired
    private MapValidationErrorService mapValidationErrorService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(elevatorRequestValidator);
    }

    /**
     * Ping service to config if we are alive.
     *
     * @return String pong
     */
    @GetMapping(value = "/ping")
    public String ping() {

        return "pong";
    }

    /**
     * Send request for elevator as POST request
     * eg.
     *
     * {
     * 	"floor":"1"
     * }
     *
     * @param elevatorRequestDto
     * @param result
     * @return
     */
    @PostMapping(value = "/request")
    public ResponseEntity<?> requestElevator(@Valid @RequestBody ElevatorRequestDto elevatorRequestDto, BindingResult result){

        Optional<ResponseEntity<?>> errorMap = mapValidationErrorService.mapResult(result);

        if(errorMap.isPresent()){
            return errorMap.get();
        }
        Elevator elevator = elevatorController.requestElevator(elevatorRequestDto);
        ElevatorResponseDto elevatorResponseDto = new ElevatorResponseDto(elevator);

        return new ResponseEntity<>(elevatorResponseDto, HttpStatus.CREATED);
    }

    /**
     * Stop threads of elevators
     * @return
     */
    @PostMapping(value = "/stop")
    public ResponseEntity<String> stopElevators(){
        elevatorController.stopSystem();

        return new ResponseEntity<>( "Stop signal sent to the system", HttpStatus.ACCEPTED);
    }

    /**
     * Retrieve list of all elevators
     * @return
     */
    @GetMapping(value = "/elevators")
    public ResponseEntity<List<ElevatorResponseDto>> getListOfElevators(){
        List<Elevator> elevators = elevatorController.getElevators();
        List<ElevatorResponseDto> elevatorResponseDtos = elevators.stream()
                .map(ElevatorResponseDto::new).collect(Collectors.toList());

        return new ResponseEntity<List<ElevatorResponseDto>>(elevatorResponseDtos, HttpStatus.OK);
    }

    /**
     * It is used by UI for retrieving information about app
     * @return
     */
    @GetMapping(value = "/snapshot")
    public ResponseEntity<SystemSnapshotDto> getSystemSnapshot(){
        SystemSnapshotDto systemSnapshotDto = new SystemSnapshotDto();

        systemSnapshotDto.setElevatorList(elevatorController.getElevators());

        return new ResponseEntity<SystemSnapshotDto>(systemSnapshotDto, HttpStatus.OK);
    }

}
