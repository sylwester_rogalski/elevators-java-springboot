package com.tingco.codechallenge.elevator.controller;

import com.tingco.codechallenge.elevator.api.ElevatorController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public final class UiController {

    @Autowired
    ElevatorController elevatorController;

    /**
     * Just simple ui website for previewing system status.
     * It uses polling for updating status of UI. Websockets can be used instead.
     */
    @GetMapping(value = "/")
    public String ui() {
        return "snapshot";
    }
}
