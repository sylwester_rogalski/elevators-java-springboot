package com.tingco.codechallenge.elevator.api.impl;

import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.api.ElevatorController;
import com.tingco.codechallenge.elevator.config.AppConfig;
import com.tingco.codechallenge.elevator.dto.ElevatorRequestDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

@Component
public class ElevatorControllerImpl implements ElevatorController {

    private static final Logger logger = LoggerFactory.getLogger(ElevatorControllerImpl.class);

    int numberOfElevators;
    int maxNumberOfFloors;

    BeanFactory beanFactory;
    AppConfig appConfig;

    List<Elevator> elevators = new ArrayList<>();

    @Autowired
    public ElevatorControllerImpl(BeanFactory beanFactory, AppConfig appConfig) {
        numberOfElevators = appConfig.getNumberOfElevators();

        this.appConfig = appConfig;
        this.beanFactory = beanFactory;
        this.maxNumberOfFloors = appConfig.getMaxNumberOfFloors();

        //starts elevator threads
        IntStream.range(0, numberOfElevators).forEach((i) ->
                {
                    Elevator elevator = beanFactory.getBean(Elevator.class);
                    new Thread(elevator).start();
                    elevators.add(elevator);
                }
        );
    }

    // private methods


    /**
     * Searching for closest elevator for given elevator request
     * Under consideration is taken how far elevator is from the given floor
     * and if elevator is going in right direction
     * @param elevatorRequestDto
     * @return Elevator
     */
    private Elevator findElevatorForRequest(ElevatorRequestDto elevatorRequestDto) {

        Elevator closestElevator = null;
        int floorNumber = elevatorRequestDto.getFloor();
        int closestFloor = 0;
        int closest = appConfig.getMaxNumberOfFloors() + 1; //set max
        Elevator currentElevator = null;
        int currentFloorNumber = 0;

        for (int i = 0; i < elevators.size(); i++) {
            currentElevator = (Elevator) elevators.get(i);
            currentFloorNumber = currentElevator.getCurrentFloor();
            if (currentElevator.getDirection() == Elevator.Direction.UP) {
                if (currentFloorNumber > closestFloor && currentFloorNumber < floorNumber) {
                    closestElevator = currentElevator;
                    closestFloor = currentFloorNumber;
                }
            } else if (currentElevator.getDirection() == Elevator.Direction.DOWN) {
                if (currentFloorNumber < closestFloor && currentFloorNumber > floorNumber) {
                    closestElevator = currentElevator;
                    closestFloor = currentFloorNumber;
                }
            } else if (floorNumber != currentElevator.getCurrentFloor() && currentElevator.getDirection() == Elevator.Direction.NONE) {
                if (Math.abs(currentFloorNumber - floorNumber) < closest) {
                    closestElevator = currentElevator;
                    closest = Math.abs(currentFloorNumber - floorNumber);
                }
            }
        }

        //better logic could be applied like waiting for free elevator before scheduling next request
        return closestElevator == null ? currentElevator : closestElevator;
    }

    // public methods

    @Override
    public Elevator requestElevator(ElevatorRequestDto elevatorRequestDto) {
        logger.debug("New request arrived: floor " + elevatorRequestDto.getFloor());

        Elevator elevator = findElevatorForRequest(elevatorRequestDto);
        elevator.setAddressedFloors(elevatorRequestDto.getFloor());

        return elevator;
    }


    @Override
    public List<Elevator> getElevators() {
        return elevators;
    }


    @Override
    public void stopSystem() {
        //would be worth send elevators on floor 0 before stopping them
        elevators.forEach(Elevator::stopRunning);
    }


    @PreDestroy
    public void preDestroy() {
        stopSystem();
    }

}
