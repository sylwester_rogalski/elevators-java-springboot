package com.tingco.codechallenge.elevator.api;

import com.tingco.codechallenge.elevator.dto.ElevatorRequestDto;

import java.util.List;


/**
 * Interface for the Elevator Controller.
 *
 * @author Sven Wesley
 *
 */
public interface ElevatorController {

    /**
     * Request an elevator to the specified floor.
     *
     * @param elevatorRequestDto
     *            addressed floor as integer.
     * @return The Elevator that is going to the floor, if there is one to move.
     */
    Elevator requestElevator(ElevatorRequestDto elevatorRequestDto);

    /**
     * A snapshot list of all elevators in the system.
     *
     * @return A List with all {@link Elevator} objects.
     */
    List<Elevator> getElevators();

    /**
     * Stops elevators from receiving next requests
     */
    void stopSystem();
}
