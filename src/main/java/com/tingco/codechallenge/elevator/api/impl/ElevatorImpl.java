package com.tingco.codechallenge.elevator.api.impl;

import com.tingco.codechallenge.elevator.api.Elevator;
import com.tingco.codechallenge.elevator.config.AppConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.IntStream;

import static java.lang.Thread.sleep;

@Component
@Scope(value = "prototype")
public class ElevatorImpl implements Elevator, Runnable {

    private static final Logger logger = LoggerFactory.getLogger(ElevatorImpl.class);

    private static Integer counter = 0;

    private Integer floorTravelTime = 0;
    private Integer doorActionTime = 0;
    private Integer maxNumberOfFloors = 0;

    // ---
    private Integer id = 0;
    private Integer currentFloor = 0;
    private Direction direction = Direction.NONE;
    private boolean[] addressedFloors;

    private Integer highestFloor = 0;
    private Integer lowestFloor = 0;

    // ---
    private volatile boolean isRunning;


    @Autowired
    public ElevatorImpl(AppConfig theAppConfig) {
        id = ++counter;

        maxNumberOfFloors = theAppConfig.getMaxNumberOfFloors();
        floorTravelTime = theAppConfig.getFloorTravelTime();
        doorActionTime = theAppConfig.getDoorActionTime();

        addressedFloors = new boolean[maxNumberOfFloors];

        debug("Created: travelTime: " + floorTravelTime + ", doorActionTime: " + doorActionTime);    }

    private void debug(String message) {
        logger.debug(String.format("Elevator [id: %d][floor: %d] %s", id, currentFloor, message));
    }

    @Override
    synchronized public Direction getDirection() {
        return direction;
    }

    @Override
    synchronized public int getCurrentFloor() {
        return currentFloor;
    }

    @Override
    synchronized public List<Integer> getAddressedFloors() {
        List<Integer> floors = new ArrayList<>();
        IntStream.range(0, addressedFloors.length).forEach(i -> {
            if(addressedFloors[i]){
                floors.add(i);
            }
        });

        return floors;
    }

    @Override
    synchronized public int getId() {
        return id;
    }

    @Override
    synchronized public void setAddressedFloors(int toFloor) {
        debug("ordered on floor " + toFloor);

        changeAddresedFloor(toFloor, true);
    }

    @Override
    synchronized public boolean isBusy() {
        if (direction.equals(Direction.NONE))
            return false;

        return true;
    }

    // thread related methods

    @Override
    synchronized public void stopRunning() {
        debug("Stopping");
        isRunning = false;
    }

    @Override
    synchronized public boolean isRunning() {
        return isRunning;
    }

    /**
     * Infinite loop of elevator's thread
     */
    @Override
    public void run() {
        isRunning = true;

        while (isRunning) {

            switch (direction) {
                case NONE:
                    if (hasAddressedFloors()) { // if any addressed floors in queue
                        debug("Starting travel");

                        if (addressedFloors[currentFloor]) {
                            arrived();
                            continue;
                        }

                        this.direction = calculateNewDirection();
                    }
                    break;
                case UP:
                case DOWN:
                    if (addressedFloors[currentFloor]) { //current floor is one which was addressed
                        arrived();
                    } else {
                        travel(); //if not then travel
                    }
                    break;
            }

        }

        debug("Stopped");
    }

    // private methods

    //simulation of doing action
    private void delay(long time) {
        try {
            sleep(time);
        } catch (InterruptedException ix) {
            logger.error(ix.getMessage());
        }
    }

    //simulating opening door
    private void openDoor() {
        debug("Opening door");
        //it would be worth checking if there is not too much people inside (weight check)
    }

    //simulating closing door
    private void closeDoor() {
        debug("Closing door");
    }

    /**
     * It is called when elevator got to requested floor
     */
    private void arrived() {
        debug("Arrived to floor");

        openDoor();
        closeDoor();

        changeAddresedFloor(currentFloor, false); //mark the floor as visited
        verifyDirectionAfterArrival(); //check if it was last addressed floor and do action regarding that
    }

    /**
     * Change status of addressed floor
     * it is used for updating array of addressed floors
     * additionally it calls methods which updates currently the highest and lowest addressed floor
     *
     * @param floor
     * @param isRequested
     */
    private void changeAddresedFloor(int floor, boolean isRequested) {
        addressedFloors[floor] = isRequested;
        updateHighestAndLowestFloor();
    }

    private void verifyDirectionAfterArrival() {
        if (!hasAddressedFloors()) {
            direction = Direction.NONE;
            debug("No new addressed elevatorRequests");
        } else {

            if (direction == Direction.UP && highestFloor < currentFloor)
                direction = Direction.DOWN;

            if (direction == Direction.DOWN && lowestFloor > currentFloor)
                direction = Direction.UP;
        }
    }

    private void travel() {

        delay(floorTravelTime);

        if (direction == Direction.UP) {
            moveElevatorUp();
        } else {
            moveElevatorDown();
        }

        debug("");
    }

    private void moveElevatorUp() {
        currentFloor = currentFloor < maxNumberOfFloors - 2 ? currentFloor + 1 : maxNumberOfFloors - 1;
    }

    private void moveElevatorDown() {
        currentFloor = currentFloor > 0 ? currentFloor - 1 : 0;
    }

    /**
     * If there are few requests in the queue then calculate which direction is best
     * There is taken under consideration number of floors above and below elevator
     *
     * @return Direction
     */
    private Direction calculateNewDirection() {
        List<Integer> requestsAbove = new ArrayList<>();
        List<Integer> requestsBelow = new ArrayList<>();

        IntStream.range(0, maxNumberOfFloors).forEach(index -> {
            if (addressedFloors[index]) {
                if (index > currentFloor)
                    requestsAbove.add(index);
                else
                    requestsBelow.add(index);
            }
        });

        return requestsAbove.size() > requestsBelow.size() ? Direction.UP : Direction.DOWN;
    }

    /**
     * Calculating highest and lowest floor in addressed floors for future usage
     */
    private void updateHighestAndLowestFloor() {
        Set<Integer> set = new HashSet<>();

        IntStream.range(0, maxNumberOfFloors).filter(floor -> addressedFloors[floor]).sorted().forEach(set::add);

        if (set.size() > 0) {
            lowestFloor = Collections.min(set);
            highestFloor = Collections.max(set);
        } else {
            lowestFloor = highestFloor = currentFloor;
        }
    }


    /**
     * If Elevator has any addressed floors in queue
     *
     * @return boolean
     */
    private boolean hasAddressedFloors() {
        return IntStream.range(0, maxNumberOfFloors).anyMatch(index -> addressedFloors[index]);
    }

    @Override
    public String toString() {
        return "ElevatorImpl{" +
                "floorTravelTime=" + floorTravelTime +
                ", doorActionTime=" + doorActionTime +
                ", maxNumberOfFloors=" + maxNumberOfFloors +
                ", id=" + id +
                ", currentFloor=" + currentFloor +
                ", direction=" + direction +
                ", addressedFloors=" + Arrays.toString(addressedFloors) +
                ", highestFloor=" + highestFloor +
                ", lowestFloor=" + lowestFloor +
                ", isRunning=" + isRunning +
                '}';
    }
}
