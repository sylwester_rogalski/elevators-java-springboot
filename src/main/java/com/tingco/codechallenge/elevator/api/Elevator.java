package com.tingco.codechallenge.elevator.api;

import java.util.List;

/**
 * Interface for an elevator object.
 *
 * @author Sven Wesley
 */
public interface Elevator extends Runnable {

    /**
     * Enumeration for describing elevator's direction.
     */
    enum Direction {
        UP, DOWN, NONE
    }

    /**
     * Tells which direction is the elevator going in.
     *
     * @return Direction Enumeration value describing the direction.
     */
    Direction getDirection();

    /**
     * If the elevator is moving. This is the target floor.
     *
     * @return primitive integer number of floor
     */
    List<Integer> getAddressedFloors();

    /**
     * Get the Id of this elevator.
     *
     * @return primitive integer representing the elevator.
     */
    int getId();

    /**
     * Command to move the elevator to the given floor.
     *
     * @param toFloor int where to go.
     */
    void setAddressedFloors(int toFloor);

    /**
     * Check if the elevator is occupied at the moment.
     *
     * @return true if busy.
     */
    boolean isBusy();

    /**
     * Reports which floor the elevator is at right now.
     *
     * @return int actual floor at the moment.
     */
    int getCurrentFloor();


    /**
     * Check if elavator thread is running
     *
     * @return int actual floor at the moment.
     */
    boolean isRunning();

    /**
     * Stop elevator thread
     *
     * @return int actual floor at the moment.
     */
    void stopRunning();


    /**
     * Implementation of runnable impl
     *
     * @return int actual floor at the moment.
     */
    @Override
    void run();
}
