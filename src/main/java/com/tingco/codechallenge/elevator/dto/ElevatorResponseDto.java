package com.tingco.codechallenge.elevator.dto;

import com.tingco.codechallenge.elevator.api.Elevator;

import java.util.List;

public class ElevatorResponseDto {
    private Integer id;
    private Integer currentFloor;
    private Elevator.Direction direction = Elevator.Direction.NONE;
    private List<Integer> addressedFloors;

    public ElevatorResponseDto(Integer id, Integer currentFloor, Elevator.Direction direction, List<Integer> addressedFloors) {
        this.id = id;
        this.currentFloor = currentFloor;
        this.direction = direction;
        this.addressedFloors = addressedFloors;
    }

    public ElevatorResponseDto(Elevator elevator){
        this.id = elevator.getId();
        this.currentFloor = elevator.getCurrentFloor();
        this.direction = elevator.getDirection();
        this.addressedFloors = elevator.getAddressedFloors();
    }

    public ElevatorResponseDto(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCurrentFloor() {
        return currentFloor;
    }

    public void setCurrentFloor(Integer currentFloor) {
        this.currentFloor = currentFloor;
    }

    public Elevator.Direction getDirection() {
        return direction;
    }

    public void setDirection(Elevator.Direction direction) {
        this.direction = direction;
    }

    public List<Integer> getAddressedFloors() {
        return addressedFloors;
    }

    public void setAddressedFloors(List<Integer> addressedFloors) {
        this.addressedFloors = addressedFloors;
    }

    @Override
    public String toString() {
        return "ElevatorResponseDto{" +
                "id=" + id +
                ", currentFloor=" + currentFloor +
                ", direction=" + direction +
                ", addressedFloors=" + addressedFloors +
                '}';
    }
}
