package com.tingco.codechallenge.elevator.dto;

import com.tingco.codechallenge.elevator.api.Elevator;

import java.util.List;

public class SystemSnapshotDto {
    List<Elevator> elevatorList;

    public List<Elevator> getElevatorList() {
        return elevatorList;
    }

    public void setElevatorList(List<Elevator> elevatorList) {
        this.elevatorList = elevatorList;
    }

    // additional information can be added like max number of floors
}
