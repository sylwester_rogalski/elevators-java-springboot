package com.tingco.codechallenge.elevator.dto;

import org.springframework.stereotype.Component;

@Component
public class ElevatorRequestDto {


    ElevatorRequestDto() {
    }

    public ElevatorRequestDto(int floor) {
        this.floor = floor;
    }

    private int floor;

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

}
