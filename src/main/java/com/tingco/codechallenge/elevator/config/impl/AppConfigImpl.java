package com.tingco.codechallenge.elevator.config.impl;

import com.tingco.codechallenge.elevator.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;

@Configuration
@PropertySources({ @PropertySource("classpath:application.properties") })
public class AppConfigImpl implements AppConfig {

    @Autowired
    private Environment env;

    @Override
    public int getFloorTravelTime(){
        return env.getProperty("com.tingco.elevator.floorTravelTime", Integer.class);
    }

    @Override
    public int getDoorActionTime(){
        return env.getProperty("com.tingco.elevator.doorActionTime", Integer.class);
    }

    @Override
    public int getNumberOfElevators() {
        int numberOfElevators = env.getProperty("com.tingco.elevator.numberofelevators", Integer.class);
        return numberOfElevators == 0 ? 1 : numberOfElevators;
    }

    @Override
    public int getMaxNumberOfFloors() {
        return env.getProperty("com.tingco.elevator.maxNumberOfFloors", Integer.class);
    }
}
