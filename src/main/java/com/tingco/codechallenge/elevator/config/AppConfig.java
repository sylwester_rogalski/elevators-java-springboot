package com.tingco.codechallenge.elevator.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public interface AppConfig {

    int getFloorTravelTime();

    int getDoorActionTime();

    int getNumberOfElevators();

    int getMaxNumberOfFloors();
}
