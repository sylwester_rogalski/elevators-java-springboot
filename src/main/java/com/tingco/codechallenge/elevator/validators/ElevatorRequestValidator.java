package com.tingco.codechallenge.elevator.validators;

import com.tingco.codechallenge.elevator.config.AppConfig;
import com.tingco.codechallenge.elevator.dto.ElevatorRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Validator for ElevatorRequest dto
 */
@Component
public class ElevatorRequestValidator implements Validator {

    @Autowired
    AppConfig appConfig;

    @Override
    public boolean supports(Class<?> aClass) {
        return ElevatorRequestDto.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        int maxNumberOfFloors = appConfig.getMaxNumberOfFloors();
        ValidationUtils.rejectIfEmpty(errors, "floor", "floor.empty");
        ElevatorRequestDto elevatorRequestDto = (ElevatorRequestDto) o;

        if (elevatorRequestDto.getFloor() >= maxNumberOfFloors) {
            errors.rejectValue("floor", "exceedes max number of floors: " + maxNumberOfFloors);
        } else if (elevatorRequestDto.getFloor() < 0) {
            errors.rejectValue("floor", "given floor is lower than 0");
        }
    }
}
