package com.tingco.codechallenge.elevator.services;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
public class MapValidationErrorService {

    /**
     * Mapping validation result for more concise response
     * @param result
     * @return
     */
    public Optional<ResponseEntity<?>> mapResult(BindingResult result) {
        if (result.hasErrors()) {

            Map<String, String> errorMap = new HashMap<>();

            for (FieldError error : result.getFieldErrors()) {
                errorMap.put(error.getField(), error.getCode());
            }

            return Optional.of(new ResponseEntity<>(errorMap, HttpStatus.BAD_REQUEST));
        }

        return Optional.empty();
    }

}

