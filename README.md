# Elevator Coding Challenge

Simple elevator simulator

Simple UI of the app is on default address "localhost:8080/"
There are visible created elevators on simple params about them.
UI is refreshed by polling data using AJAX requests on /rest/v1/snapshot endpoint.


There are four REST endpoints for posting and retrieving data.

/rest/v1/snapshot

It retrieves list of elevators but there was idea to return more information

eg.

```
{
    "elevatorList": [
        {
            "id": 1,
            "currentFloor": 0,
            "direction": "NONE",
            "addressedFloors": [],
            "busy": false,
            "running": true
        },
        {
            "id": 2,
            "currentFloor": 0,
            "direction": "NONE",
            "addressedFloors": [],
            "busy": false,
            "running": true
        }
    ]
}
```

#
/rest/v1/elevators

GET retrieves list of elevators

eg.


```
[
    {
        "id": 1,
        "currentFloor": 0,
        "direction": "NONE",
        "addressedFloors": []
    },
    {
        "id": 2,
        "currentFloor": 2,
        "direction": "UP",
        "addressedFloors": []
    }
]
```


#
/rest/v1/stop

stops elevator's threads

#
/rest/v1/request

POST data on this endpoint for requesting elevator

eg. 
```{"floor":"9"}```



##Summary

Hope to improve this app in near future